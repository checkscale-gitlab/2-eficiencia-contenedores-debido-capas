# Influencia de las imágenes en la eficiencia de los contenedores

## Abstract

Docker esta siendo la tecnología de creación de contenedores más extendida, gracias a la independencia que ofrece para la ejecución de varios procesos y aplicaciones, conservando cierta seguridad. Cuando se traslada cierta implementación Docker a producción, dependiendo de las aplicaciones o procesos de cada contenedor, hay que conocer los aspectos más importantes que afecten al rendimiento y desempeño.

En este trabajo se propone proporcionar una serie de recomendaciones y conclusiones que ayuden a la implementación óptima de aplicaciones o procesos en Docker. Durante el trabajo se van a exponer distintas características y diferentes aspectos que se han abordado a la hora de realizar las pruebas y los resultados obtenidos. Para finalmente concluir con unas conclusiones y poder informar sobre la mejor recomendación de configuración en caso de pasar a la fase de producción.

## Objetivos

Existen diferentes aspectos al tener en cuenta cuando se traslada un sistema creado a producción. Es importante conocer los aspectos más relevantes a tener en cuenta y tener en mente las ventajas o desventajas que puedan tener.

Por tanto, el objetivo principal del estudio, es aportar información clara y concisa sobre aspectos a tener en cuenta a la hora realizar una implementación final a producción de manera optima.

Para ello, este estudio extrae la información basada en la teoría y resultados obtenidos, abordando aspectos tales como:

1. Las capas de una imagen
2. Tipo y tamaño de fichero que se tratan
3. Distintas operaciones que se realizan en dichos ficheros
4. Controlador de almacenamiento utilizado
5. Número de instancias lanzadas

Los resultados son obtenidos mediante la simulación de un entorno lo más fiel posible a la realidad y mediante el uso de tests de manera automatizada para lograr obtener el mayor número de resultados posibles.

## Metodología

Para poder abordar los objetivos se han creado una serie de tests que serán ejecutados sobre un contenedor. Los tests son lanzados de forma automatizada mediante diferentes shell scripts predefinidos o mediante el uso de Pyhton. Tanto el lanzamiento de los test como del propio contenedor es realizado de forma automática con Shell script. En general, el lanzamiento de un test es realizado mediante el arranque de un contenedor con el test a realizar. De esta manera se mejora la precisión de los resultados de cada test.

Los resultados obtenidos son almacenados en una carpeta de resultados con una nomenclatura de nombre claro para facilitar su posterior análisis. Además  con ayuda de una serie de fichero en Pyhton son generadas gráficas a partir de los resultados obtenidos y favorecer de manera visual el análisis de los resultados. Para más detalle sobre la metodología seguida y código utilizado, visitar el repositorio en Gitlab[^4].

[^4]: Servidor git donde se ubica el desarrollo del código implementado: https://git.upv.es/mamarbao/trabajo-de-investigaci-n-cloud-computing

Posteriormente en este documento, se profundizará respecto al entorno experimental y herramienta de pruebas.

## Discusión

Antes de proceder a detallar la implementación de los tests, el entorno experimental y cargas de trabajo utilizadas, es necesario conocer un poco de información acerca de como Docker realiza creación y uso de imágenes. De este modo, se puede conocer y entender los diferentes aspectos a tener en cuenta en la creación y lanzamiento de los test, así como las muestras o carga de trabajo utilizadas.

La creación de imágenes en Docker se basa principalmente de aplicar capa sobre capa de lectura cada una de las instrucciones definidas en el Dockerfile[[1]](#1). Cuando se crea un contenedor a partir de la imagen, se añade ha esta pila una nueva capa con acceso de escritura (capa contenedora),por tanto, cada contenedor tiene su propia capa de escritura y todos los cambios son almacenados en esta.

Esta metodología permite que existan varias imágenes basadas en capas similares ( mismos paquetes, SO etc..), todas estas capas comunes son almacenadas una vez ahorrando en memoria.

Quien se encarga de la implementación de esta metodología de capas y su gestión es el controlador de almacenamiento[[2]](#1). En este caso, Docker acepta los siguientes controladores: aufs, overlay, overlay2, btrfs y otros más reciente como ZFS [[3]](#1). Este último esta más pensado para contenedores que deban trabajar con un tamaño de ficheros muy elevado, el cual se les reserva un bloque de memoria y trabaja mediante el uso de *snapshots* en vez de capas.

Estos administran el contenido de las capas de imágenes y la capa de escritura. Cada uno de los controladores  disponibles manejan estas capas de forma diferente y utilizan una estrategia de copia e escritura llamada CoW, para lograr compartir y copiar archivos lo más eficientemente posible. Generalizando, básicamente se basa en permitir leer directamente ens archivos de capas inferiores y en caso de necesitar escribir/modificar un fichero de una capa inferior, es copiado a la capa actual y se modifica,

Para el entorno experimental y creación de los test se ha tenido aspectos como:

- El número de capa (profundidad) donde se opera
- Tipo de fichero y tamaño
- Controlador de almacenamiento que actúa

La finalidad es lograr observar como afectan estos aspectos a la latencia, rendimiento y operaciones de escritura y lectura. Para ello se ha creado una imagen Base y una imagen basada en esta para la realización de las pruebas. En la imagen de pruebas esta creada de forma que se puedan abarcar los aspectos anteriormente comentados.

En caso, de desear entrar en detalle, más información en el siguiente repositorio: [Metodología Código Readme Gitlab](https://git.upv.es/mamarbao/trabajo-de-investigaci-n-cloud-computing)

## Resultados

Los resultados han sido obtenidos como se describe en el siguiente repositorio: [Como se han obtenido los resultado](https://git.upv.es/mamarbao/trabajo-de-investigaci-n-cloud-computing)

Antes de observar los resultado, hay que tener en cuenta los siguientes aspectos:

- Cada test ha sido lanzado 10 veces y los resultados son obtenidos mediante la media de los 10 resultados obtenidos.
- Los resultados son utilizando el controlador de almacenamiento **overlay2**. Este controlador es aplicado por Docker por defecto cuando es instalado por primera vez.
- Posteriormente se comparan los resultados de **overlay2** con **aufs** y **overlay**.
- La máquina de pruebas contiene un ssd nvme con altas velocidades de escritura y de lectura
- Se ha realizado bajo la versión de docker 19.03.14

Por último, las gráficas de resultados a lo largo del documento contienen una leyenda con unos valores numéricos, estos corresponden al número de capa de la pila. Siendo el número 0 la capa de escritura o capa contenedora.

### Diferentes tamaños de fichero

A continuación se procede a comparar los resultados obtenidos con el primer test. Estos primeros resultados observamos como afecta el número de capas en operaciones de lectura y de escritura. En este caso, utilizando ficheros de distintos tamaños.

<img src="./python-benchmark-graphs/graphs/mygraph-read-overlay2.png" style="zoom:70%;" />


Como se plasma en la gráfica, los tiempos con ficheros de pequeño tamaño apenas varia el tiempo de lectura. Esto es debido a que todas las capas tiene permisos de lectura y por tanto, no debería de afectar la ubicación de los ficheros leídos considerablemente. Si que se aprecian cambios bruscos en la lectura de fichero de 32 y 128, en las capas superiores o incluso de los ficheros ubicados en la carpeta del volumen. Aquí se observa como la implementación de capas por parta de Docker y Overlay2 esta pensada para optimizar los tiempos de lectura, al mismo tiempo que se reduce el tamaño entre imágenes similares. Ahora con operaciones de escritura:

<img src="./python-benchmark-graphs/graphs/mygraph-write-overlay2.png" style="zoom:70%;" />

Los tiempos de escritura mostrados en esta gráfica, conforme aumenta la profundidad de la capa donde se trabaja, mayor es el tiempo de escritura. Esto es debido a la metodología de CoW aplicada, antes de escribir cada fichero debe ser copiado a la capa contenedora (escritura), esto conlleva un mayor tiempo en los primeros acceso al fichero. 

### Mismo tamaño de fichero

Ahora realizamos las operaciones de lectura y escritura sobre otra distinta muestra. Para el segundo test se procede a realizar operaciones de lectura y escritura sobre una carpeta con 10000 ficheros de 512 bytes. Como en el caso anterior, se realizan las pruebas para las diferentes capas.

A continuación los resultados obtenidos para overlay2:

<img src="./python-benchmark-graphs/graphs/mygraph-write-small-files-overlay2.png" style="zoom:70%;" />

Como representanta la gráfica, con un número mas elevado de fichero pero más reducidos, la diferencia en tiempo de escritura es todavía mas significativo, es decir, la metodología *CoW* afecta más significativamente con volúmenes con gran cantidad de ficheros de menor tamaño. 

En caso de operaciones de lectura, se mantienen tiempos similares entre todas las capas.

<img src="./python-benchmark-graphs/graphs/mygraph-read-small-files-overlay2.png" style="zoom:70%;" />

### Latencia y Rendimiento

En aspectos como la latencia y el rendimiento ha sido utilizado el comando **dd** de manera debida para que los resultados reflejen dichos aspectos. Para más detalles sobre su implementación, consultar el siguiente enlace: [Test throughou y latency](https://git.upv.es/mamarbao/trabajo-de-investigaci-n-cloud-computing) Con el controlador Overlay2, se han obtenido los siguientes resultados:

<img src="./python-benchmark-graphs/graphs/mygraph-throughput-overlay2.png" style="zoom:70%;" />

En temas de rendimiento se observa que no existe demasiada variación entre el uso de capas a diferentes profundidades, la diferencia podría algo más significante si se hubiera realizado bajo un disco mecánico. En esta caso la maquina de pruebas cuenta con SSD M2  a un máximo de 700 MB/S en lectura y escritura.  Pero aquí se observa claramente como el uso de un contenedor si que afecta al rendimiento si lo comparamos con la máquina que sería el volumen (*Bind mount*). La reducción sería entorno a un 20% de media.

<img src="./python-benchmark-graphs/graphs/mygraph-latency-overlay2.png" style="zoom:70%;" />

Si ahora se observa los resultados de la latencia, estos concuerdan con los resultado con el rendimiento. A menor latencia, mayor es el rendimiento generalmente. Vemos como en este caso a mayor profundidad de capa mayor es la latencia, aunque la variación entre ellas no supera los 2 ms. La latencia en este caso aumenta entorno a un 30% en el peor de los casos.

### Diferentes UFS

Docker permite utilizar diferentes tipos de UFS, en este trabajo se han realizado pruebas sobre overlay, overlay2 y aufs.

 En el apartado anterior se han mostrado los resultados bajo overlay2, a continuación los resultados obtenidos con todos los tests ejecutados bajo aufs,overlay y overlay2:

<img src="./python-benchmark-graphs/graphs/mygraph-write-overall.png" style="zoom:100%;" />

![](./python-benchmark-graphs/graphs/mygraph-read-overall.png)

Estos resultados en operaciones de escritura y lectura en las distintas implementaciones de UFS, muestran como overlay2 proporciona los mejores tiempos tanto de lectura como de escritura, independientemente del tamaño del fichero y de las distintas ubicaciones o capas donde se localizan. Además en aufs y overlay el número de capas tiene un mayor efecto en los tiempos. Por último, son comparados los 3 tipos de controladores en términos de latencia y rendimiento:

<img src="./python-benchmark-graphs/graphs/mygraph-throughput-overall.png" style="zoom:70%;" />

<img src="./python-benchmark-graphs/graphs/mygraph-latency-overall.png" style="zoom:70%;" />

Los resultados de estas dos gráficas confirman que en términos generales, overlay2 ofrece mejores resultados en latencia y el controlador aufs mejores resultados en rendimiento. Pero cabe destacar que no existe una gran diferencia entre los 3 controladores.

### Efectos bajo E/S en paralelo

Para concluir las pruebas se ha comprado el efecto de realizar múltiples operaciones de entrada y salida simultáneamente dentro de los contenedores. Esto se ha realizado modificando y poniendo en marcha un benchmark [[4]](#1). ya creado por el usuario de github  [chriskuehl](https://github.com/chriskuehl)[^3]

En este caso se ha adaptado para que realice las mismas operaciones de lectura y de escritura de los apartados anteriores y manteniendo una prueba original del creador donde se realizan pruebas sobre ficheros de tipo *binary tree* bajo operaciones de lectura y de escritura.

A continuación los resultados obtenidos más relevantes:

<img src="./python-benchmark-graphs/graphs/graph-append-to-small-files.png" style="zoom:70%;" />

En operaciones de escritura con una cantidad elevada de ficheros, los tiempos son mejores en aufs, pero a medida que aumenta el número de procesos en paralelo se puede apreciar como overlay y overlay2 van mejorando respecto aufs quien va aumentado el tiempo a medida que aumenta el número de procesos.

<img src="./python-benchmark-graphs/graphs/graph-read-small-files.png" style="zoom:70%;" />

En la lectura es más notorio como aufs va reduciendo su desempeño a medida que aumenta el número de procesos.

Para finalizar con los pruebas, se han realizado operaciones de lectura y de escritura sobre un árbol binario[^1] dentro del contenedor. El test utilizado realiza DFS[^2] para ir sumergiéndose dentro del árbol, realizando "*readlink*" en los directorios intermedios, hasta alcanzar un nodo hoja, entonce se realiza la lectura del fichero.

<img src="./python-benchmark-graphs/graphs/graph-read-file-tree.png" style="zoom:70%;" />

<img src="./python-benchmark-graphs/graphs/graph-append-to-file-tree.png" alt="graph-append-to-file-tree" style="zoom:70%;" />

Como ocurre con ficheros de menor tamaño, a medida que aumenta la paralización IO mejores resultados ofrecen overlay y overlay. Por el contrario, el controlador aufs ofrece peores resultados debido al incremento de la paralización.

[^1]: Serie de nodos con dos ramas, estas ramas pueden terminar en otro nodo o una hoja.
[^2]: Una **Búsqueda en profundidad** mediante un algoritmo de búsqueda, para recorrer todos los nodos de un árbol ordenadamente
[^3]: [chriskuehl](https://github.com/chriskuehl)  / **[docker-storage-benchmark](https://github.com/chriskuehl/docker-storage-benchmark)**

## Conclusiones:

Con toda la información aportado, no se puede concluir aportando un caso ideal, en su lugar, hay una serie de alternativas optimas dependiendo en este caso, del uso futuro de los ficheros en un contenedor.  A continuación una serie de conclusiones como resultado de las pruebas realizadas.

En operaciones solo de lectura no afecta considerablemente  tener los ficheros localizados en las capas más inferiores, pero si es recomendable tener los ficheros en las capas superiores en caso de un volumen elevado de ficheros con un pequeño tamaño.

En operaciones de escritura que se llevan acabo con poca frecuencia, es recomendable tener los ficheros en las capas superiores, siendo todavía mejor alternativa si estos ficheros son de un tamaño considerable de más de 32 MB. Si se tiene que realizar muchas escrituras sobre una gran cantidad de ficheros, no es nada recomendable que estos están ubicados en alguna de las capas internar.

En operaciones de escritura de mayor frecuencia que hagan uso de una gran cantidad de ficheros pequeños, el controlador **aufs** es la mejor alternativa. En caso de permanecer en el controlador por defecto **overlay2**, es recomendable tener estos fichero en la capa más superior o en la capa de escritura o contenedora.

Cuando existen varios procesos simultáneos sobre los ficheros, el controlador **aufs** es quien se ve afectada negativamente siendo más favorable el uso de **overlay** o **overlay2**.

En comparación con las 3 implementaciones de **ufs** probadas, **overaly2** es quien ofrece mejores resultados en tiempos de lectura independientemente de la capa. En tiempos de escritura solo en el caso de tratar con un número elevado de ficheros pequeños, es más recomendable utilizar **aufs**. También cabe destacar que el desempeño del volumen compartido, es mejor cuando se utiliza **overlay2**.

## Bibliografía:

<a id="1">[1]</a> 
Docs Docker
docker images
https://docs.docker.com/engine/reference/commandline/images/

<a id="1">[2]</a> 
Docs Docker
Manage data in Docker
https://docs.docker.com/storage/

<a id="1">[3]</a> 
[ardnaxelarak](https://github.com/ardnaxelarak)
Docker and ZFS in practice
https://github.com/Shopify/docker/blob/master/docs/userguide/storagedriver/zfs-driver.md

<a id="1">[4]</a> 
[chriskuehl](https://github.com/chriskuehl/docker-storage-benchmark/commits?author=chriskuehl)
docker-storage-benchmark
https://github.com/chriskuehl/docker-storage-benchmark

<a id="1">[5]</a> 
Docs Docker
Use volumes
https://docs.docker.com/storage/volumes/



