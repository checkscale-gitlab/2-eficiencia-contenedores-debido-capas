\contentsline {section}{Influencia de las imágenes en la eficiencia de los contenedores}{2}{section*.2}%
\contentsline {subsection}{Abstract}{2}{section*.3}%
\contentsline {subsection}{Objetivos}{2}{section*.4}%
\contentsline {subsection}{Metodología}{3}{section*.5}%
\contentsline {subsection}{Discusión}{3}{section*.6}%
\contentsline {subsection}{Resultados}{4}{section*.7}%
\contentsline {subsubsection}{Diferentes tamaños de fichero}{4}{section*.8}%
\contentsline {subsubsection}{Mismo tamaño de fichero}{6}{section*.9}%
\contentsline {subsubsection}{Latencia y Rendimiento}{8}{section*.10}%
\contentsline {subsubsection}{Diferentes UFS}{10}{section*.11}%
\contentsline {subsubsection}{Efectos bajo E/S en paralelo}{13}{section*.12}%
\contentsline {subsection}{Conclusiones:}{15}{section*.13}%
\contentsline {subsection}{Bibliografía:}{16}{section*.14}%
