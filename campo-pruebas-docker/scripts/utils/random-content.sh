#!/bin/bash
#$1 total files created
#$2 File used to create new files
#$3 If *stop* then deleted all files, If not *stop* then path destination files
# Launch: ./random-content.sh 10 Infinite4k.png 
filepath=/tmp/

delete_all_files() {
find $filepath -type f -name 'temporal_file[0-9].[0-9]*' -exec rm {} +
}

if [[ $3 == *stop* || $1 == *delete* ]];then
        delete_all_files > /dev/null 2>&1
		exit 0
elif [[ ! -z $3  ]];then
        filepath=$3
fi

filecount=0
while [ $filecount -lt $1 ] ; do
    filesize=$RANDOM
    filesize=$(($filesize+1024))
    ts=$(date +%s%N); 
    base64 $2 | 
    head -c "$filesize" > ${filepath}temporal_file${filecount}.$RANDOM
    tt=$((($(date +%s%N) - $ts)/1000000)) ; 
    echo "[$tt]ms [/tmp/file${filecount}.$RANDOM]:$filesize"
    ((filecount++))
done


