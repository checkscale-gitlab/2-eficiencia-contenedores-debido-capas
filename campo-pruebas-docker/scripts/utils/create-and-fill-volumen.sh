#!/bin/bash
nameVolumen=data_test
checkerror() {
	RESULT=$1
	if [ $RESULT != 0 ];then
		echo "[ERROR] Errors occured while creating voluemn docker: $nameVolumen"
		exit 127
	fi
}

docker volume create $nameVolumen
checkerror $?
mkdir -p /var/lib/docker/volumes/$nameVolumen/_data/small-files
mkdir -p /var/lib/docker/volumes/$nameVolumen/_data/big-files
mkdir -p /var/lib/docker/volumes/$nameVolumen/_data/generate-files-container
# Big files.
for i in $(seq 0 9); do
	for size in 1M 8M 32M 128M; do
		dd if=/dev/urandom of=/var/lib/docker/volumes/$nameVolumen/_data/big-files/${size}-${i}.bin bs=${size} count=1 2>/dev/null;
	done;
done

# small files 512 bytes
for i in $(seq 0 10000); do \
    dd if=/dev/urandom of=/var/lib/docker/volumes/$nameVolumen/_data/small-files/$i bs=512 count=1 2>/dev/null; \
done
