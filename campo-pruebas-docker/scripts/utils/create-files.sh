#!/bin/bash -eu
bigfolder=$1
smallfolder=$2
for f in $1 $2; do
    if [ -z "$f"  ];then echo "[EXIT] Not name directory";exit 127;fi
    if [ ! -d "$f"  ];then echo "[EXIT] Directory $f does not exists.";exit 127;fi
done
#Big files  1M 8M 32M 128M
for i in $(seq 0 9); do \
        for size in 1M 8M 32M 128M; do \
            dd if=/dev/urandom of=$bigfolder/${size}-${i}.bin bs=${size} count=1 2>/dev/null; \
        done; \
    done

#Small files 512 Bytes
for i in $(seq 0 10000); do \
        dd if=/dev/urandom of=$smallfolder/$i bs=512 count=1 2>/dev/null; \
    done
