#!/bin/bash -eu
folder=$1
if [ -z "$folder"  ];then echo "[EXIT] Not name directory";exit 127;fi
if [ ! -d "$folder"  ];then echo "[EXIT] Directory $folder does not exists.";exit 127;fi

#/var/lib/docker/volumes/$nameVolumen/_data/big-files/
#/test/big-folder/
#/test/small-files/

ts=$(date +%s%N);    
for f in "$folder"*; do
    cat "$f" > /dev/null
done
tt=$((($(date +%s%N) - $ts)/1000000)) ; 
echo "$tt" #ms