#!/bin/bash -eu
#/path/namefolder/
folder=$1
#Sizes in this test: 1 8 32 128
size=$2
if [ -z "$folder"  ];then echo "[EXIT] Not name directory";exit 127;fi
if [ ! -d "$folder"  ];then echo "[EXIT] Directory $folder does not exists.";exit 127;fi

#/var/lib/docker/volumes/$nameVolumen/_data/big-files/
#/test/big-folder/
#/test/small-files/

ts=$(date +%s%N);    
for f in "$folder"$size"M"*; do
    cat "$f" > /dev/null
done
tt=$((($(date +%s%N) - $ts)/1000000)) ; 
echo "$tt" #ms